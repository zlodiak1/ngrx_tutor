import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Car } from '../car.model'

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {

  carName: string = '';
  carModel: string = '';

  @Output() addCar = new EventEmitter<Car>();

  private id: number = 2;

  constructor() { }

  ngOnInit() {
  }

  onAdd() {
      if(this.carModel === '' || this.carName === '') { return }
      this.id = this.id + 1;

      console.log(this.id)

      const car = new Car(
          this.carName,
          'date',
          this.carModel,
          false,
          this.id
      );

      this.addCar.emit(car);

      this.carModel = '';
      this.carName = '';
  }

  onLoad() {

  }

}
