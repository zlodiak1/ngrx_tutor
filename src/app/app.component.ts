import { Component, OnInit } from '@angular/core';
import { Car } from './car.model'
import { Store } from '@ngrx/store'
import { AppState } from './redux/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    public cars: Car[] = [];

    constructor(private store: Store<AppState>) {

    }

    onAdd(car: Car) {
        console.log('onAddonAdd')
        this.cars.push(car);
    }

    onDelete(car: Car) {
        this.cars = this.cars.filter(c => c.id !== car.id);
    }
}
