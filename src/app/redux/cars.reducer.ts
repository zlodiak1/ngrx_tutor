import { Car } from '../car.model';
import { Action } from '@ngrx/store';
import { CAR_ACTION, AddCar } from './cars.action';


const initialState = {
    cars: [
        new Car('ford', '12.12.12', 'focus', false, 1),
        new Car('ford2', '12.12.18', 'focus2', false, 2),  
    ]  
}

export function carsReducer(state = initialState, action: AddCar) {
    switch (action.type) {
        case CAR_ACTION.ADD_CAR:
            return {
                ...state,
                cars: [...state.cars, action.payload]
            };
        default: 
            return state;
    }
}